import 'package:flutter/material.dart';

class SecondMobile extends StatefulWidget {
  const SecondMobile({Key? key}) : super(key: key);

  @override
  State<SecondMobile> createState() => _SecondMobileState();
}

class _SecondMobileState extends State<SecondMobile> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Text('Drei einfache Schritte zur Vermittlung neuer Mitarbeiter')
        ],
      ),
    );
  }
}
