import 'package:flutter/material.dart';

class MobileFirst extends StatefulWidget {
  const MobileFirst({Key? key}) : super(key: key);

  @override
  State<MobileFirst> createState() => _MobileFirstState();
}

class _MobileFirstState extends State<MobileFirst> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: const [
         Text('Drei einfache Schritte zur Vermittlung neuer Mitarbeiter', style: TextStyle(fontSize: 21),textAlign: TextAlign.center)
        ],
      ),
    );
  }
}
