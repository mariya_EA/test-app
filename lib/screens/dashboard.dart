import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:test_app/screens/second_tab_mobile.dart';
import 'package:test_app/screens/third_tab_mobile.dart';

import 'first_tab_mobile.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        bottomOpacity: 1.0,
        actions: const [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15.0),
            child: Text(
              'Login',
              style: TextStyle(
                  color: Color(0xff319795), fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              alignment: AlignmentDirectional.topCenter,
              children: [
                SvgPicture.asset(
                  'assets/images/undraw_agreement_aajr.svg',
                  // width: 18.0,
                  // height: 18.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Text(
                    'Deine Job website',
                    style: TextStyle(fontSize: 42, color: Color(0xff2D3748)),
                  ),
                )
              ],
            ),
            Container(
                height: 80,
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10))),
                child: Card(
                    child: SizedBox(
                  height: 20,
                  width: 20,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      ElevatedButton(
                        child: Text(
                          "Kostenlos Registrieren",
                          style: TextStyle(fontSize: 20),
                        ),
                        onPressed: () {},
                        style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll<Color>(
                                Color(0xff3182CE))),
                      )

                      // ElevatedButton(
                      //   onPressed: () { },
                      //
                      //   child: Ink(
                      //     decoration:  BoxDecoration(
                      //        boxShadow: [
                      //       BoxShadow(
                      //         color: Colors.grey,
                      //         offset: Offset(0.0, 1.5),
                      //         blurRadius: 1.5,
                      //       ),
                      //     ],
                      //       borderRadius: BorderRadius.all(Radius.circular(80.0)),
                      //     ),
                      //     child: Container(
                      //       constraints: const BoxConstraints(minWidth: 88.0, minHeight: 36.0), // min sizes for Material buttons
                      //       alignment: Alignment.center,
                      //       child: const Text(
                      //         'OK',
                      //         textAlign: TextAlign.center,
                      //       ),
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                ))),
            SizedBox(
              height: MediaQuery.of(context).size.height,
              child: DefaultTabController(
                length: 3, // length of tabs
                initialIndex: 0,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        height: 50,
                        // E6FFFA
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xff81E6D9),
                        ),

                        child: TabBar(
                          indicatorColor: Colors.green,
                          indicatorWeight: 1,
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.white,
                          unselectedLabelStyle: TextStyle(color: Colors.green),
                          tabs: [
                            Tab(
                                child: Container(
                              height: 400,
                              width: 800,
                              color: Color(0xff81E6D9),
                              child: const Center(
                                  child: Text(
                                'Arbeitnehmer',
                              )),
                            )),
                            Tab(
                                child: Container(
                              height: 400,
                              width: 800,
                              color: Color(0xff81E6D9),
                              child: const Center(
                                  child: Text(
                                'Arbeitgeber',
                              )),
                            )),
                            Tab(
                                child: Container(
                              height: 400,
                              width: 800,
                              color: Color(0xff81E6D9),
                              child: const Center(
                                  child: Text(
                                'Temporärbüro',
                              )),
                            )),
                          ],
                        ),
                      ),
                      Container(
                        height: 400, //height of TabBarView
                        decoration: const BoxDecoration(
                            border: Border(
                                top: BorderSide(
                                    color: Colors.grey, width: 0.5))),
                        child: const TabBarView(children: <Widget>[
                          MobileFirst(),
                          SecondMobile(),
                          ThirdScreen()
                        ],),
                      )
                    ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
